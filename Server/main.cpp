#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 63455
#define BUFSIZE 2048

int main() {
    std::cout << "Hello, Server!" << std::endl;

    struct sockaddr_in myaddr;
    struct sockaddr_in remaddr;
    socklen_t addrlen = sizeof(remaddr);
    int recvlen; 
  	int fd;
  	unsigned int alen;
    int msgcnt = 0;
    unsigned char recv_buf[BUFSIZE];

	  if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		  perror("Cannot create socket");
		  return 1;
  	}

    std::cout << "Created socket (descriptor: " << fd << ")" << std::endl;

  	memset((void *)&myaddr, 0, sizeof(myaddr));
  	myaddr.sin_family = AF_INET;
  	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  	myaddr.sin_port = htons(PORT);

	  if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
		  perror("bind failed");
	  	return 1;
	  }

	  alen = sizeof(myaddr);
	  if (getsockname(fd, (struct sockaddr *)&myaddr, &alen) < 0) {
		  perror("getsockname failed");
		  return 1;
	  }

    std::cout << "Bind complete (port: " << ntohs(myaddr.sin_port) << ")" << std::endl;
    
    for (;;) {
      std::cout << "Listening on port " << PORT << std::endl;
      recvlen = recvfrom(fd, recv_buf, BUFSIZE, 0, (struct sockaddr *)&remaddr, &addrlen);
      std::cout << "Recieved " << recvlen << " bytes" << std::endl;
      if (recvlen > 0) {
        recv_buf[recvlen] = 0;
        std::cout << "Recieved message: \"" << recv_buf << "\"" << std::endl;
      }
		  char const *res = "Recieved packet";
      std::cout << "Sending response: \"" << res << "\"" << std::endl;
      if (sendto(fd, res, strlen(res), 0, (struct sockaddr *)&remaddr, addrlen) < 0)
        perror("sendto failed");
    }
    
    return 0;
}
