Single Build Process
===

In Server or Client directory:

```
$ cmake -H. -Bbuild
$ cmake --build build -- -j3
```

Output in `./bin`

or run `$ . ./build.sh`

