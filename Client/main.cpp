#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define BUFLEN 2048
#define MSGS 5
#define PORT 63455

int main() {
  std::cout << "Hello, Client!" << std::endl;

  struct sockaddr_in myaddr;
  struct sockaddr_in remaddr;
  socklen_t slen = sizeof(remaddr);
	int fd;
  int i; // temp for MSGS loop
  int recvlen;
	char const *server = "127.0.0.1";
	char buf[BUFLEN];

  if ((fd=socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("Cannot create socket");
    return 1;
  }

	memset((char *)&myaddr, 0, sizeof(myaddr));
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	myaddr.sin_port = htons(0);

	if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
		perror("bind failed");
		return 1;
	}

	memset((char *) &remaddr, 0, sizeof(remaddr));
	remaddr.sin_family = AF_INET;
	remaddr.sin_port = htons(PORT);
	if (inet_aton(server, &remaddr.sin_addr)==0) {
		fprintf(stderr, "inet_aton() failed\n");
		exit(1);
	}

	for (i=0; i < MSGS; i++) {
		printf("Sending packet %d to %s port %d\n", i, server, PORT);
		sprintf(buf, "This is packet %d", i);
		if (sendto(fd, buf, strlen(buf), 0, (struct sockaddr *)&remaddr, slen)==-1)
			perror("sendto");
    recvlen = recvfrom(fd, buf, BUFLEN, 0, (struct sockaddr *)&remaddr, &slen);
    if (recvlen >= 0) {
      buf[recvlen] = 0;
      printf("received message: \"%s\"\n", buf);
    }
	}

	return 0;
}
